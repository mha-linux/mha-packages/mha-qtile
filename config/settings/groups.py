from libqtile.config import Key, Group, Match
from libqtile.command import lazy
from .keys import mod, keys
from libqtile import qtile

groups = [
	Group("   ", matches=[Match(wm_class=["Navigator"])]),
	Group("  ", matches=[Match(wm_class=["Gedit"])]),
	Group("   ", matches=[Match(wm_class=["TelegramDesktop", "Whatsapp-for-linux"])]),
	Group("   ", matches=[Match(wm_class=["Spotify", "vlc"])]),
	Group("   ", matches=[Match(wm_class=["Gimp-2.10"])]),
	Group("   ", matches=[Match(wm_class=["vlc"])]),
	Group("   ", matches=[Match(wm_class=["Mail"])]),
	Group("   "),
]

custom_keys = ["a", "z", "e", "r", "t", "y", "u", "i"]

for i, group in enumerate(groups):
    keys.extend([
        Key(["mod1"], "Tab", lazy.screen.next_group()),
        Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),
        Key(["mod1", "control"], custom_keys[i], lazy.window.togroup(group.name, switch_group=True)),
        Key(["mod1", "shift"], custom_keys[i], lazy.group[group.name].toscreen())
    ])
