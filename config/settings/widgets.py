from libqtile import widget
from .theme import colors

def base(fg='text', bg='dark'): 
    return {
        'foreground': colors[fg],
        'background': colors[bg]
    }


def separator():
    return widget.Sep(**base(), linewidth=0, padding=5)


def icon(fg='text', bg='dark', fontsize=16, text="?"):
    return widget.TextBox(
        **base(fg, bg),
        fontsize=fontsize,
        text=text,
        padding=3
    )


def powerline(fg="light", bg="dark"):
    return widget.TextBox(
        **base(fg, bg),
        text='', # left triangle
        fontsize=43,
        padding=-15
    )


def workspaces(): 
    return [
        separator(),
        widget.GroupBox(
            **base(fg='light'),
            font='UbuntuMono Nerd Font',
            fontsize=19,
            margin_y=3,
            margin_x=0,
            padding_y=8,
            padding_x=5,
            borderwidth=1,
            active=colors['active'],
            inactive=colors['inactive'],
            rounded=False,
            highlight_method='block',
            urgent_alert_method='block',
            urgent_border=colors['urgent'],
            this_current_screen_border=colors['focus'],
            this_screen_border=colors['grey'],
            other_current_screen_border=colors['dark'],
            other_screen_border=colors['dark'],
            disable_drag=True
        ),
        separator(),
        widget.WindowName(**base(fg='focus'), fontsize=14, padding=5),
        separator(),
    ]


primary_widgets = [
    *workspaces(),

    separator(),


    widget.Systray(background=colors['dark'], padding=5),
    powerline('color4', 'dark'),
    widget.Wttr(background=colors["color4"], padding=10, location={'Tunis': 'Home'}, foreground=colors["dark"]),
    powerline('color1', 'color4'),
    icon(bg="color1", text='  '),
    # widget.MemoryGraph(**base(bg='color1'), padding=10, type="line"),
    widget.Memory(**base(bg='color1'), padding=10, fontsize=12),
    powerline('color2', 'color1'),
    icon(bg="color2", text='  '),
    # widget.CPUGraph(**base(bg='color2'), padding=10, type="line"),
    widget.CPU(**base(bg='color2'), format="CPU {load_percent}%", padding=10, fontsize=12),
    
    powerline('color4', 'color2'),

    icon(bg="color4", text='  '), # download
    
    widget.CheckUpdates(
        background=colors['color4'],
        colour_have_updates="#2d8200",
        colour_no_updates="#820000",
        no_update_string='0',
        display_format='{updates}',
        update_interval=1800,
        custom_command='pacman -Qu',
        execute='alacritty -e sudo pacman -Syu', fontsize=12
    ),

    powerline('color3', 'color4'),

    icon(bg="color3", text='  '), # feed
    
    widget.Net(**base(bg='color3'), prefix='M', format='{down}↓', fontsize=12),

    powerline('color2', 'color3'),

    widget.CurrentLayoutIcon(**base(bg='color2'), scale=0.65),

    widget.CurrentLayout(**base(bg='color2'), padding=5, fontsize=12),

    powerline('color1', 'color2'),

    icon(bg="color1", fontsize=17, text='  '), # calendar

    widget.Clock(**base(bg='color1'), format='%d/%m/%Y - %H:%M ', timezone="Africa/Tunis", fontsize=12),

    powerline('light', 'color1'),
    widget.KeyboardLayout(background=colors["light"], foreground=colors['dark'], configured_keyboards=["fr","ara"], padding=10, fontsize=12),
    powerline('dark', 'light'),
    widget.QuickExit(background=colors["dark"], fontsize=12, default_text="  ", countdown_format="{} s", padding=5)
]

secondary_widgets = [
    *workspaces(),

    separator(),

    powerline('color1', 'dark'),

    widget.CurrentLayoutIcon(**base(bg='color1'), scale=0.65),

    widget.CurrentLayout(**base(bg='color1'), padding=5),

    powerline('color2', 'color1'),

    widget.Clock(**base(bg='color2'), format='%d/%m/%Y - %H:%M '),

    powerline('dark', 'color2'),
]

widget_defaults = {
    'font': 'UbuntuMono Nerd Font Bold',
    'fontsize': 14,
    'padding': 1,
}
extension_defaults = widget_defaults.copy()
