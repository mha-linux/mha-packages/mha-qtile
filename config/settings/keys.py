# Antonio Sarosi
# https://youtube.com/c/antoniosarosi
# https://github.com/antoniosarosi/dotfiles

# Qtile keybindings

from libqtile.config import Key, Drag, Click
from libqtile.command import lazy


mod = "mod4"

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
    Click([mod], "Button1", lazy.screen.next_group())
]

keys = [Key(key[0], key[1], *key[2:]) for key in [
    # ------------ Window Configs ------------

    # Switch between windows in current stack pane
    ([mod], "k", lazy.layout.down()),
    ([mod], "i", lazy.layout.up()),
    ([mod], "j", lazy.layout.left()),
    ([mod], "l", lazy.layout.right()),	

    # Change window sizes (MonadTall)
    ([mod, "shift"], "o", lazy.layout.grow()),
    ([mod, "shift"], "l", lazy.layout.shrink()),

    # Toggle floating
    ([mod, "control"], "f", lazy.window.toggle_floating()),

    # Move windows up or down in current stack
    ([mod, "shift"], "i", lazy.layout.shuffle_down()),
    ([mod, "shift"], "k", lazy.layout.shuffle_up()),

    # Toggle between different layouts as defined below
    ([mod, "control"], "Tab", lazy.next_layout()),
    
    # Set window to fullscreen
    ([mod, "shift"], "f", lazy.window.toggle_fullscreen()),

    # Kill window
    ([mod], "q", lazy.window.kill()),

    # Switch focus of monitors
    ([mod], "n", lazy.next_screen()),
    ([mod], "v", lazy.prev_screen()),

    # Power menu
    ([mod, "shift"], "q", lazy.spawn("rofi -show power-menu -modi power-menu:rofi-power-menu")),
    
    # Lock screen
    ([mod, "shift"], "l", lazy.spawn("betterlockscreen -l")),
    
    # Suspend
    ([mod, "control"], "l", lazy.spawn("systemctl suspend & betterlockscreen -l")),

    # Restart Qtile
    ([mod, "shift"], "r", lazy.restart()),

    ([mod, "control"], "q", lazy.shutdown()),
    
    # Change keyboard
    ([mod, "mod1"], "Tab", lazy.widget["keyboardlayout"].next_keyboard()),

    # ------------ App Configs ------------

    # Menu
    ([mod], "r", lazy.spawn("rofi -show drun")),

    # Window Nav
    ([mod], "Tab", lazy.spawn("rofi -show")),

    # Browser
    ([mod], "b", lazy.spawn("firefox")),

    # File Explorer
    ([mod], "f", lazy.spawn("pcmanfm")),

    # Terminal
    ([mod], "Return", lazy.spawn("alacritty")),

    # ------------ Hardware Configs ------------

    # Volume
    ([mod], "o", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),
    ([mod], "l", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%")),
    ([mod], "p", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ toggle")),
    ([], "XF86AudioLowerVolume", lazy.spawn(
        "pactl set-sink-volume @DEFAULT_SINK@ -5%"
    )),
    ([], "XF86AudioRaiseVolume", lazy.spawn(
        "pactl set-sink-volume @DEFAULT_SINK@ +5%"
    )),
    ([], "XF86AudioMute", lazy.spawn(
        "pactl set-sink-mute @DEFAULT_SINK@ toggle"
    )),

    # Brightness
    ([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +10%")),
    ([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 10%-")),
]]
